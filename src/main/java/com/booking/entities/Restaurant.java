package com.booking.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString 
@Getter 
@Setter
@Entity
@Table( name = "RESTAURANT" )
public class Restaurant 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID",unique = true,nullable = false)
	private Long id;
	
	@Column(name = "NAME" )
	private String name;
	
	@Column(name = "ADDRESS" )
	private String address;
	
	@Column(name = "DESCRIPTION" )
	private String description;
	
	@Column(name = "IMAGE" )
	private String image;
	
	//añadimos relacion con reserva , en modo de cascada, consulta en modo LAzy)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "restaurant")
	List<Reservation> reservation;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "restaurant")
	List<Board> borads;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "restaurant")
	List<Turn> turns;

	
 
	
	
	

}
