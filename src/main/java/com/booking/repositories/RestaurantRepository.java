package com.booking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.booking.entities.Restaurant;


@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long>
{

	//Ponemos un Optinal para que nunca nos devuelva un nulo
	Optional<Restaurant> findById(Long id);
	
	Optional<Restaurant> findByName(String nameRestaurant);
	
//	Optional<Restaurant> findById(Long id);
	
	//podemos poner querys
	@Query("SELECT REST FROM Restaurant REST")
	public List<Restaurant> findRestaurants();	
}
