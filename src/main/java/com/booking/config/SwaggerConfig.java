package com.booking.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final Contact DEFAULT_CONTAC = new Contact("Ing.Mauricio-GS", 
															 "https://github.com/gomomauricio",
															 "gomomauricio@gmail.com");
	
	
	
	//proveedores de la pagina
		public static final List<VendorExtension> VENDOR_EXTENSION = new ArrayList<VendorExtension>();
		
		
		public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
                "Booking Restaurant servicio para manejo de reservaciones", 
                "Este servicio usa Mysql para persistencia de datos", 
                "1.0.0", 
                "Terminos de servicio", 
                DEFAULT_CONTAC, 
                "Compañia MGM LOBOS", 
                "Informacion extra de la compañia https://gitlab.com/gomomauricio", 
                VENDOR_EXTENSION);

		

	// muestra todos los controladores en el swagger
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO)
				                                      .select()
				                                      .apis( RequestHandlerSelectors.basePackage("com.booking") )
				                                      .paths(PathSelectors.any()).build();
	}
}
