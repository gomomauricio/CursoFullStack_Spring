package com.booking.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor    //todos los argumentos
@ToString
@RequiredArgsConstructor(staticName = "of")  //sin el Data
public class BookingResponse<T> implements Serializable
{ 
	private static final long serialVersionUID = 1L;
	
	@NonNull private String status;
	@NonNull private String code;
	@NonNull private String message;
	private T data;
	
}
