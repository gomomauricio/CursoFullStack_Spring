package com.booking.services;

import com.booking.exceptions.BookingExceptions;

public interface CancelReservationService 
{
	public String deleteReservation(String locator) throws BookingExceptions;

}
