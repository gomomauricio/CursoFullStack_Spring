package com.booking.services;

import java.util.List;

import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.RestaurantRest;

public interface RestaurantService 
{
	
	RestaurantRest getRestaurantById(Long restaurantId) throws BookingExceptions;
	
	public List<RestaurantRest> getRestaurants() throws BookingExceptions;

}
