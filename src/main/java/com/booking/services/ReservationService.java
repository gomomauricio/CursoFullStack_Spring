package com.booking.services;

import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.CreateReservationRest;
import com.booking.jsons.ReservationRest;

public interface ReservationService 
{
	ReservationRest getReservation(Long reservationId) throws BookingExceptions;
	
	String createReservation(CreateReservationRest createReservationRest) throws BookingExceptions;

}
