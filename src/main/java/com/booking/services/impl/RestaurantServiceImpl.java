package com.booking.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.booking.entities.Restaurant;
import com.booking.exceptions.BookingExceptions;
import com.booking.exceptions.NotFoundException;
import com.booking.jsons.RestaurantRest;
import com.booking.repositories.RestaurantRepository;
import com.booking.services.RestaurantService;

@Service
public class RestaurantServiceImpl implements RestaurantService {
	@Autowired
	RestaurantRepository restaurantRepository;

	public static final ModelMapper modelMApper = new ModelMapper();

	public RestaurantRest getRestaurantById(Long restaurantId) throws BookingExceptions {
		// para que mapee la identidad DAO a la identidad JSON
		return modelMApper.map(getRestaurantEntity(restaurantId), RestaurantRest.class);
	}

	public List<RestaurantRest> getRestaurants() throws BookingExceptions {

		// traemos todos los restaurantes
		final List<Restaurant> restaurantsEntity = restaurantRepository.findAll();

		return restaurantsEntity.stream().map(service -> modelMApper.map(service, RestaurantRest.class))
				.collect(Collectors.toList());
	}

	private Restaurant getRestaurantEntity(Long restaurantId) throws BookingExceptions {
		return restaurantRepository.findById(restaurantId)
				.orElseThrow(() -> new NotFoundException("SNOT-404-1", "RESTAURANT_NOT_FOUND"));
	}

}
