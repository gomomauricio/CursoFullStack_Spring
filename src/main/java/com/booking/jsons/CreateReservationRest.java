package com.booking.jsons;

import java.util.Date;

import com.booking.response.BookingResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateReservationRest
{
	@JsonProperty("date")   
	private Date date;
	
	@JsonProperty("person")   
	private String person;
	
	@JsonProperty("turnId")   
	private Long turnId;
	
	@JsonProperty("restaurantId")   
	private Long restaurantId;
  
}
