package com.booking.jsons;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantRest 
{
	//para mandar datos json de respuesta
	//contine TODOS los campos que tienen nuestras identidades
	
	@JsonProperty("id")  //esto es lo que muestra al front
	private Long id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("address")
	private String address;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("image")
	private String image;
	
	@JsonProperty("turn")
	private List<TurnRest> turns;

	 
	

}
