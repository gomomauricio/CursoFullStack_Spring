package com.booking.exceptions;

import java.util.Arrays;

import org.springframework.http.HttpStatus;

import com.booking.dtos.ErrorDto;

public class InternalServerErrorException extends BookingExceptions
{
	//para manejar los errores que ocurren al insertar o eliminar en la base de datos
	private static final long serialVersionUID = 1L;

	public InternalServerErrorException(String code, String message) {
		super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
	}
	
	public InternalServerErrorException(String code, String message, ErrorDto data) {
		super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message, Arrays.asList(data) );
	}
	

}
