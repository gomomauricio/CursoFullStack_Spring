package com.booking.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.booking.dtos.ErrorDto;

public class BookingExceptions extends Exception
{
	private static final long serialVersionUID = 1L;

	//
	private final String code;
	
	private final Integer responseCode;
	
	private final List<ErrorDto> errorList = new ArrayList<>();
	
	

	public BookingExceptions(String code, Integer responseCode,String message) 
	{
		super(message);
		this.code = code;
		this.responseCode = responseCode;
	}
	
	public BookingExceptions(String code, Integer responseCode,String message,List<ErrorDto> errorList ) 
	{
		super(message);
		this.code = code;
		this.responseCode = responseCode;
		this.errorList.addAll(errorList);
	}

	public String getCode() {
		return code;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public List<ErrorDto> getErrorList() {
		return errorList;
	}
	
	
	
	
	

}
