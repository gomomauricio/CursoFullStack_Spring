package com.booking.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.CreateReservationRest;
import com.booking.response.BookingResponse;
import com.booking.services.ReservationService;
import com.booking.services.impl.ReservationServiceImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")// ANGULAR _para consumir los servicios en modo local evitar el cross
@RequestMapping(path = "/booking-restaurant" + "/v1")
public class ReservationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReservationController.class);

	@Autowired
	ReservationService reservationService;

	// produce = indicamos con que modelo de datos vamos a trabajar
	/////////////////////////////////////////////////

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "reservation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookingResponse<String> createReservation(@RequestBody CreateReservationRest createReservationRest)
			throws BookingExceptions {
		BookingResponse<String> response = null;
		String data = reservationService.createReservation(createReservationRest);
		response = new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK", data);
		return response;
	}

}
