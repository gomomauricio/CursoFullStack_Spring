package com.booking.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.booking.exceptions.BookingExceptions;
import com.booking.response.BookingResponse;
import com.booking.services.CancelReservationService;
import com.booking.services.impl.ReservationServiceImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")// ANGULAR _para consumir los servicios en modo local evitar el cross
@RequestMapping(path = "/booking-restaurant" + "/v1")
public class CancelReservationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CancelReservationController.class);

	@Autowired
	CancelReservationService cancelReservationService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "deleteReservation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookingResponse<String> deleteReservation(@RequestParam String locator) throws BookingExceptions {
		String data = cancelReservationService.deleteReservation(locator);

		return new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK", data);
	}

}
