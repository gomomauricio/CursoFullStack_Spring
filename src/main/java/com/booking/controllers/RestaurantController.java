package com.booking.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.RestaurantRest;
import com.booking.response.BookingResponse;
import com.booking.services.RestaurantService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")// ANGULAR _para consumir los servicios en modo local evitar el cross
@RequestMapping(path = "/booking-restaurant" + "/v1")
public class RestaurantController {
	@Autowired
	RestaurantService restaturantService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "restaurant" + "/{" + "restaurantId"
			+ "}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookingResponse<RestaurantRest> getRestaurantById(@PathVariable Long restaurantId) throws BookingExceptions {
		return new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK",
				restaturantService.getRestaurantById(restaurantId));
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "restaurants", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookingResponse<List<RestaurantRest>> getRestaurants() throws BookingExceptions {
		List<RestaurantRest> data = restaturantService.getRestaurants();

		return new BookingResponse<>("Succes", String.valueOf(HttpStatus.OK), "OK", data);
	}

}
