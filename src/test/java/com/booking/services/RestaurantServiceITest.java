package com.booking.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.booking.entities.Board;
import com.booking.entities.Reservation;
import com.booking.entities.Restaurant;
import com.booking.entities.Turn;
import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.RestaurantRest;
import com.booking.jsons.TurnRest;
import com.booking.repositories.RestaurantRepository;
import com.booking.services.impl.RestaurantServiceImpl;

public class RestaurantServiceITest {
	
	private static final Long RESTAURANT_ID = 1L;
	private static final String NAME = "Burger";
	private static final String DESCRIPTION = "Todo tipo de hamburgesas";
	private static final String ADDRES = "Av. Galindo";
	private static final String IMAGE = "www.image.com";
	
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	private static final Restaurant RESTAURANT = new Restaurant();
	private static final List<Turn> TURNS = new ArrayList<Turn>();
	private static final List<Board> BOARD_LIST = new ArrayList<Board>();
	private static final List<Reservation> RESERVATIONS_LIST = new ArrayList<Reservation>();
	private static final List<Restaurant> RESTAURANT_LIST = new ArrayList<Restaurant>();
	
	@Mock
	RestaurantRepository restaurantRepository;
	
	@InjectMocks
	RestaurantServiceImpl restaurantService;
	
	
	@BeforeEach
	public void init() throws BookingExceptions
	{
		MockitoAnnotations.initMocks(this);
		
		RESTAURANT.setName(NAME);
		RESTAURANT.setDescription(DESCRIPTION);
		RESTAURANT.setAddress(ADDRES);
		RESTAURANT.setId(RESTAURANT_ID);
		RESTAURANT.setImage(IMAGE);
		RESTAURANT.setTurns(TURNS);
		RESTAURANT.setBorads(BOARD_LIST);
		RESTAURANT.setReservation(RESERVATIONS_LIST);
		
		RESTAURANT_LIST.add(RESTAURANT);
	}
	
	@Test
	public void getRestaurantByIdTest() throws BookingExceptions
	{
		Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.of( RESTAURANT ));
		restaurantService.getRestaurantById(RESTAURANT_ID);
	}
	
	@Test 
	public void getRestaurantByIdTestError() throws BookingExceptions
	{
		Mockito.when(restaurantRepository.findById(RESTAURANT_ID)).thenReturn(Optional.empty());
		restaurantService.getRestaurantById(RESTAURANT_ID);
		fail();
	}
	
	
	@Test
	public void getRestaurantsTest() throws BookingExceptions
	{
		Mockito.when(restaurantRepository.findAll()).thenReturn(RESTAURANT_LIST);
		final List<RestaurantRest> response = restaurantService.getRestaurants();
		assertNotNull(response);
		assertFalse(response.isEmpty());
		assertEquals(response.size(), 1);
		
	}
	

}
