package com.booking.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.booking.entities.Board;
import com.booking.entities.Reservation;
import com.booking.entities.Restaurant;
import com.booking.entities.Turn;
import com.booking.exceptions.BookingExceptions;
import com.booking.exceptions.NotFoundException;
import com.booking.jsons.CreateReservationRest;
import com.booking.repositories.ReservationRepository;
import com.booking.repositories.RestaurantRepository;
import com.booking.repositories.TurnRepository;
import com.booking.services.impl.ReservationServiceImpl;

public class ReservationServiceImplTest {
	
	private static final CreateReservationRest CREATE_RESERVATION_REST = new CreateReservationRest(); 
	private static final String PERSON = "Persona";
	private static final Long RESTAURANT_ID = 1L;
	private static final Long RESERVATION_ID = 1L;
	private static final Long TURN_ID = 1L;
	private static final Date DATE = new Date();
	private static final Restaurant RESTAURANT = new Restaurant();
	
	private static final String NAME = "Burger";
	private static final String LOCATOR = "TURN_12_004";
	private static final String DESCRIPTION = "Todo tipo de hamburgesas";
	private static final String ADDRES = "Av. Galindo";
	private static final String IMAGE = "www.image.com";
	
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	private static final List<Turn> TURNS = new ArrayList<Turn>();
	private static final List<Board> BOARD_LIST = new ArrayList<Board>();
	private static final List<Reservation> RESERVATIONS_LIST = new ArrayList<Reservation>();
	private static final List<Restaurant> RESTAURANT_LIST = new ArrayList<Restaurant>();
	private static final Optional<Restaurant> OPTIONAL_RESTAURANT = Optional.of(RESTAURANT);
	private static final Optional<Restaurant> OPTIONAL_RESTAURANT_EMPTY = Optional.empty();
	private static final Optional<Reservation> OPTIONAL_RESERVATION_EMPTY = Optional.empty();
	private static final Reservation RESERVATION = new Reservation();
	private static final Optional<Reservation> OPTIONAL_RESERVATION = Optional.of(RESERVATION);
	private static final Turn TURN = new Turn();
	private static final Optional<Turn> OPTIONAL_TURN = Optional.of(TURN);
	
	@Mock
	private RestaurantRepository restaurantRepository;

	@Mock
	private TurnRepository turnRepository;

	@Mock
	private ReservationRepository reservationRepository;
	
	@InjectMocks
	private ReservationServiceImpl reservationServiceImpl;
	
	
	
	@BeforeEach
	public void init() throws BookingExceptions
	{
		MockitoAnnotations.initMocks(this);
		
		CREATE_RESERVATION_REST.setDate(DATE);
		CREATE_RESERVATION_REST.setPerson(PERSON);
		CREATE_RESERVATION_REST.setRestaurantId(RESTAURANT_ID);
		CREATE_RESERVATION_REST.setTurnId(TURN_ID);
		
		RESERVATION.setDate(DATE);
		RESERVATION.setId(RESERVATION_ID);
		RESERVATION.setLocator(LOCATOR);
		RESERVATION.setPerson(PERSON);
		RESERVATION.setRestaurant(RESTAURANT);
		RESERVATION.setTurn(TURN.getName());
		
		TURN.setId(TURN_ID);
		TURN.setName(NAME);
		TURN.setRestaurant(RESTAURANT);
		
		RESTAURANT.setName(NAME);
		RESTAURANT.setDescription(DESCRIPTION);
		RESTAURANT.setAddress(ADDRES);
		RESTAURANT.setId(RESTAURANT_ID);
		RESTAURANT.setImage(IMAGE);
		RESTAURANT.setTurns(TURNS);
		RESTAURANT.setBorads(BOARD_LIST);
		RESTAURANT.setReservation(RESERVATIONS_LIST);
	}
	
	@Test
	public void createReservationTest() throws BookingExceptions
	{
		Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
		Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(OPTIONAL_TURN); 
		Mockito.when( reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId() )).thenReturn(OPTIONAL_RESERVATION_EMPTY);	
		
		//lo creamos siempre que hagamos un save o update
		Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
		
		reservationServiceImpl.createReservation(CREATE_RESERVATION_REST);
	}
	
	
	
	 @Test()
	public void createReservationFindByTestError() throws BookingExceptions
	{
		Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT_EMPTY);
		Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(OPTIONAL_TURN); 
		Mockito.when( reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId() )).thenReturn(OPTIONAL_RESERVATION_EMPTY);	
		
		//lo creamos siempre que hagamos un save o update
		Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
		
//		reservationServiceImpl.createReservation(CREATE_RESERVATION_REST);
//		Assertions.assertThrows(BookingExceptions.class, () -> reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
		 
		assertThrows(BookingExceptions.class, () -> reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
//		fail();
	}
	 
	 @Test()
		public void createReservationFindByTestErrorTURN() throws BookingExceptions
		{
		 Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
			
			Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(Optional.empty()); 
			
			//lo creamos siempre que hagamos un save o update
			Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
			
				 Exception exc = assertThrows(NotFoundException.class, () ->
				 reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
				 
//				 exc.printStackTrace();
//				 System.out.println("*****1" + exc.getMessage());
				 
			 assertEquals("TURN_NOT_FOUND", exc.getMessage());
		}
	 
	 @Test()
		public void createReservationFindByTestErrorEXISTE() throws BookingExceptions
		{
		   Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
			Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(OPTIONAL_TURN); 
		
			Mockito.when( reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId() )).thenReturn(OPTIONAL_RESERVATION);	
			
			//lo creamos siempre que hagamos un save o update
			Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
			
				 Exception exc = assertThrows(NotFoundException.class, () ->
				 reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
				 
//				 exc.printStackTrace();
//				 System.out.println("*****3" + exc.getMessage());
				 
			 assertEquals("RESERVATION_ALREADT_EXIST", exc.getMessage());
		}
	 
	 @Test()
		public void createReservationFindByTestErrorDOS() throws BookingExceptions
		{
		   Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT_EMPTY);
			Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(OPTIONAL_TURN); 
			Mockito.when( reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId() )).thenReturn(OPTIONAL_RESERVATION_EMPTY);	
			
			//lo creamos siempre que hagamos un save o update
			Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
			
				 Exception exc = assertThrows(NotFoundException.class, () ->
				 reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
				 
//				 exc.printStackTrace();
//				 System.out.println("*****2" + exc.getMessage());
				 
			 assertEquals("RESTAURANT_NOT_FOUND", exc.getMessage());
		}
	 
	 
	 
	 	@Test()
		public void createReservationFindByTestErrorINTERNAL() throws BookingExceptions
		{
		   Mockito.when( restaurantRepository.findById(RESTAURANT_ID)).thenReturn(OPTIONAL_RESTAURANT);
			Mockito.when( turnRepository.findById(TURN_ID)).thenReturn(OPTIONAL_TURN); 
			Mockito.when( reservationRepository.findByTurnAndRestaurantId(TURN.getName(), RESTAURANT.getId() )).thenReturn(OPTIONAL_RESERVATION_EMPTY);	
//Esta parte es la que debe fallar			
//			Mockito.when( reservationRepository.save(Mockito.any(Reservation.class))).thenReturn( new Reservation());
			Mockito.doThrow(Exception.class).when( reservationRepository).save(Mockito.any(Reservation.class));
			
				 Exception exc = assertThrows(Exception.class, () ->
				 reservationServiceImpl.createReservation(CREATE_RESERVATION_REST));
				 
//				 exc.printStackTrace();
				 System.out.println("*****internal" + exc.getMessage());
				 
			 assertEquals("INTERNAL_SERVER_ERROR", exc.getMessage());
		}

}
