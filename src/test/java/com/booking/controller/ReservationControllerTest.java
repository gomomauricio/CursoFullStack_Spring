package com.booking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.booking.controllers.ReservationController;
import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.CreateReservationRest;
import com.booking.response.BookingResponse;
import com.booking.services.ReservationService;

public class ReservationControllerTest {
	
	private static final CreateReservationRest CREATERESERVATIONREST = new CreateReservationRest(); 
	private static final String OK = "OK";
	private static final String LOCATOR = "Burguer 2";
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String PERSON = "Persona";
	private static final Long RESTAURANT_ID = 1L;
	private static final Long TURN_ID = 1L;
	private static final Date DATE = new Date();
	
	
	@Mock
	ReservationService reservationService;

	@InjectMocks
	ReservationController reservationController;
	
	@BeforeEach
	public void init() throws BookingExceptions
	{
		MockitoAnnotations.initMocks(this);
		CREATERESERVATIONREST.setDate(DATE);
		CREATERESERVATIONREST.setPerson(PERSON);
		CREATERESERVATIONREST.setRestaurantId(RESTAURANT_ID);
		CREATERESERVATIONREST.setTurnId(TURN_ID);
		
		when(reservationService.createReservation(CREATERESERVATIONREST)).thenReturn(LOCATOR);
	}
	
	@Test
	public void createReservationTest() throws BookingExceptions
	{
		final BookingResponse<String> response = reservationController.createReservation(CREATERESERVATIONREST);
		
//		String data = reservationService.createReservation(CREATERESERVATIONREST);


		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK);
		assertEquals(response.getData(), LOCATOR);
		
	}
	
}
