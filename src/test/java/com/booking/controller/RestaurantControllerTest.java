package com.booking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.booking.controllers.RestaurantController;
import com.booking.entities.Turn;
import com.booking.exceptions.BookingExceptions;
import com.booking.jsons.RestaurantRest;
import com.booking.jsons.TurnRest;
import com.booking.response.BookingResponse;
import com.booking.services.RestaurantService;

public class RestaurantControllerTest 
{
	
	private static final Long RESTAURANT_ID = 1L;
	private static final String NAME = "Burger";
	private static final String DESCRIPTION = "Todo tipo de hamburgesas";
	private static final String ADDRES = "Av. Galindo";
	private static final String IMAGE = "www.image.com";
	
	private static final String SUCCES_STATUS = "Succes";
	private static final String SUCCES_CODE = "200 OK";
	private static final String OK = "OK";
	private static final RestaurantRest RESTAURANT_REST = new RestaurantRest();
	private static final List<TurnRest> TURNS = new ArrayList<TurnRest>();
	private static final List<RestaurantRest> RESTAURANT_LIST = new ArrayList<RestaurantRest>();
	
	

	@Mock
	RestaurantService restaturantService;
	
	
	@InjectMocks
	RestaurantController restaurantController;
	
	
	
	@BeforeEach
	public void init() throws BookingExceptions
	{
		MockitoAnnotations.initMocks(this);
		RESTAURANT_REST.setName(NAME);
		RESTAURANT_REST.setDescription(DESCRIPTION);
		RESTAURANT_REST.setAddress(ADDRES);
		RESTAURANT_REST.setId(RESTAURANT_ID);
		RESTAURANT_REST.setImage(IMAGE);
		RESTAURANT_REST.setTurns(TURNS);
		
		when(restaturantService.getRestaurantById(RESTAURANT_ID)).thenReturn(RESTAURANT_REST);
	}
	
	@Test
	public void getRestaurantById() throws BookingExceptions {
		final BookingResponse<RestaurantRest> response = restaurantController.getRestaurantById(RESTAURANT_ID);
		
		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK);
		assertEquals(response.getData(), RESTAURANT_REST);
	}
	
	
	@Test
	public void getRestaurants() throws BookingExceptions
	{
		final BookingResponse<List<RestaurantRest>> response = restaurantController.getRestaurants();
		
		assertEquals(response.getStatus(), SUCCES_STATUS);
		assertEquals(response.getCode(), SUCCES_CODE);
		assertEquals(response.getMessage(), OK);
		assertEquals(response.getData(),RESTAURANT_LIST);
	}
	
}
