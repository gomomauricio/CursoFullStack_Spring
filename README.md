# Código del curso Dev FullStack con Spring Boot Java 8 y Angular 9 Typescript
# Instructor
> [Cristian Rojas](https://www.udemy.com/course/dev-fullstack-con-springboot-java-8-y-angular-9-typescript/) 
 

#Udemy
* Dev FullStack con Spring Boot Java 8 y Angular 9 Typescript

## Contiene

* Desarrollar Apis RestFull con SpringBoot Java 8 
* TypeScript con Angular 9
* SpringBoot
* Consumir APIs con Angular
* Crear código con su respectivo test unitario
* Diseñar y crear una base de datos en Mysql
* Enviar correos con template personalizados y implementar una pasarela de pagos con Stripe
 

## Descripción
     FrameWork SpringBoot, angular a detalle con los mejores practias en el desarrollo frontEnd.

     Veremos como consumir cualquier api con angular

     Este máster estará dividido en conjunto Backend y angular por niveles:

     Backend:
 
     1 Introducción a los Frameworks

     2 Entender la organización de los proyectos.
  
     3  Test unitario de todos nuestros controladores y servicios con Junit y Mockito.

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

/************* SPRING ***********************/
 *La mejor practica es regresar el estatus para todas las respuestas REST

*Madurez REST
El “modelo de madurez definido por Richardson” y descrito por Martin Fowler ayuda a 
entender mejor los principios en los que se basa el estilo de la arquitectura RESTful.


Nivel 0: [POX]{SOAP}
         los servicios que están en este nivel utilizan HTTP como un protocolo de 
		 transporte para codificar invocaciones de servicios remotos. 
		 SOAP es un buen ejemplo de esto porque se utiliza un único endpoint para todas 
		 las operaciones.
		 
Nivel 1: [RECURSOS]{/FACTURA /insertar /borrar /update / buscar  }
         los servicios que están en este grado de madurez introducen el concepto de recurso 
		 y cada recurso tiene su propio URI que permite referenciarlo y recuperarlo 
		 directamente.
		 
Nivel 2: [VERBOS HTTP]{/ clientes (GET) /1(GET) (post) (put) (delete)}
         los servicios de este nivel usan los principales métodos del protocolo HTTP 
		 (GET, POST, PUT, DELETE).
		 
Nivel 3: [HATEOAS](Hypermedia as the Engine of Application State)
				{
				“dni”:”123456778A”,
				“nombre”:”pedro”
				"links": [         
				{   "línea": "http://dominio/facturas/1A/lineas/1" }  ,
				{   "línea": "http://dominio/facturas/1A/lineas/2" }  ,
				{   "línea": "http://dominio/facturas/1A/lineas/3" }  ,
				  ]
				}
        los servicios que están en este nivel retornan enlaces que permiten al 
		 cliente descubrir operaciones y obtener referencias a otros recursos.
		 El servicio es autodescriptivo y el cliente va navegando por los resultados 
		 para invocar las operaciones.


spring maneja inyecciones de dependencias
y la inversion de control (spring hace lo necesario para levantar el servicio)


**No mas de 50 Microservicio por Eureka-SERVER

/********************************************/
/********************* Para Angular *********/
En la carpeta donde nos encontremos correr el servidor 
C:\directorio\MockitoJunit_Basico\angular-client-books>ng serve
con:
ng -serve

 ~~~


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.1.0**  _>>>_  Iniciando proyecto [  on 10 OCT, 2021 ]  
 ![Diagrama](src/main/resources/DiagramaAPP.jpg)
 